using System;
using System.Runtime.InteropServices;

namespace evasion1
{
    internal class Program
    {

        //import kernel32.dll 
        [DllImport("kernel32.dll", SetLastError = true)]
        static extern void Sleep(uint dwMilliseconds);
        //import kernel32.dll VirtualAllocExNuma
        [DllImport("kernel32.dll", SetLastError = true, ExactSpelling = true)]
        static extern IntPtr VirtualAllocExNuma(IntPtr hProcess, IntPtr lpAddress,
            uint dwSize, UInt32 flAllocationType, UInt32 flProtect, UInt32 nndPreferred);

        //allocate memory in our current process running space use GetCurrentProcess
        //allocate 0x1000 bytes of memory
        //allocate memory in the current process running space
        [DllImport("kernel32.dll")]
        static extern IntPtr GetCurrentProcess();
        // import FlsAlloc
        [DllImport("kernel32.dll")]
        static extern UInt32 FlsAlloc(IntPtr lpCallback);

        static void Main(string[] args)
        {
            // sleep call to evade emulation
            DateTime t1 = DateTime.Now;
            Sleep(4270);
            double t2 = DateTime.Now.Subtract(t1).TotalMilliseconds;
            if (t2 < 4000)
            {
                Console.WriteLine("Emulation detected!");
                Environment.Exit(0);
            }
            else
            {
                Console.WriteLine("No emulation detected!");
            }

            // Non-emulated APIs
            IntPtr memz = VirtualAllocExNuma(GetCurrentProcess(), IntPtr.Zero, 0x1000, 0x3000, 0x40, 0);
            Console.WriteLine("Allocated memory at: " + memz.ToString("X"));
            if (memz != null)
            {
                Console.WriteLine("ExNuma worked");
                rnr();
            }
            return;

            // Non-emulated APIs to evade emulation
            IntPtr p = VirtualAllocExNuma(GetCurrentProcess(), IntPtr.Zero, 0x1000, 0x3000, 0x40, 0);
            Console.WriteLine("Allocated memory at: " + p.ToString("X"));
            // Use FlsAlloc to evade emulation
            UInt32 result = FlsAlloc(IntPtr.Zero);
            if (result != 0xFFFFFFFF)
            {
                Console.WriteLine("FlsAlloc success!");
                rnr();
            }
            return;


        }

        private static void rnr()
        {
            Console.WriteLine("Shc would have ran");
        }
    }
}
